 /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 6.
    Энкодер
    Для управлением яркостью LED используется энкодер Sparkfun
    Автор Еличева Евгения
     */
 

unsigned long currentTime;
unsigned long loopTime;
const int pin_A = 12;       // pin 12
const int pin_B = 11;       // pin 11
unsigned char encoder_A;
unsigned char encoder_B;
unsigned char encoder_A_prev=0;
int light=0;

const int datapin = 7; // Вход последовательной шины данных
const int clockpin = 5; //   Вход тактовой частоты
const int latchEnable = 6; // Выход данных
const int outputEnable = 3; // Разрешение выхода
const int rExt = 4; // Программирование тока (внешний резистор) работает в инверсном режиме.
  //Описание сдвиговых регистров по ссылке http://ledlamps.ru/collection/drivers/product/mbi5168gd
byte data_0 = 255; //Количество сегментов, представленные в 10-ой системе. 255 в двоичной системе представлено как 11111111. Так как у нас восьмиразрядный сдвиговый регистр, 
    //на выходе имеем параллельно 8 сигналов.
 
void setup()  {
  // declare pin 9 to be an output:
   pinMode(datapin, OUTPUT);
    pinMode(clockpin, OUTPUT);
    pinMode(latchEnable, OUTPUT);
    pinMode(outputEnable, OUTPUT);
    pinMode(rExt, OUTPUT);
        SgtWrite() ; //Функция вывода данных в регистр
  pinMode(pin_A, INPUT);
  pinMode(pin_B, INPUT);
  currentTime = millis();
  loopTime = currentTime; 
} 
 
void loop()  {
  currentTime = millis();
  if(currentTime >= (loopTime + 5)){ // проверяем каждые 5мс (200 Гц)
    encoder_A = digitalRead(pin_A);     // считываем состояние выхода А энкодера 
    encoder_B = digitalRead(pin_B);     // считываем состояние выхода B энкодера    
    if((!encoder_A) && (encoder_A_prev)){    // если состояние изменилось с положительного к нулю
      if(encoder_B) {
          for(int light=0;light<=255; light++){//соответственно, на выходе получаем значения от 255 до 0
          Serial.println(light);// Посмотрим, какие значения получаются на выходе
          analogWrite(rExt,light)  ;
          delay(10);

      }              
      }   
      else {
        for(int light=255;light>=0; light--){ //Устанавливаем начальное значение яркости 255, и до тех  пор, пока значение light не достигнет нуля, уменьшается яркость лампы. Но, так как у нас инвертируемый порт, на выходе получаем значения от 0 до 255
    analogWrite(rExt,light) ;//Выдает ШИМ на порт //вход/выхода. 
//Описание ШИМ по ссылке
//http://arduino.ru/Tutorial/PWM
    Serial.println(light);// Посмотрим, какие значения получаются на выходе в мониторе порта 
//таблица кода ascii представления чисел по ссылке
//http://tehtab.ru/Guide/GuideMathematics/GuideMathematicsNumericalSystems/TableCodeEquivalent/
    delay(10);//Останавливает выполнение программы на заданное в параметре количество миллисекунд. Данная команда необходима, чтобы визуально увидеть результат предыдущих выполненных команд.
      }              
      }   
 
    }   
    encoder_A_prev = encoder_A;     // сохраняем значение А для следующего цикла 
     
    analogWrite(rExt, light);   // устанавливаем яркость 
    
    loopTime = currentTime;
  }                       
}

void SgtWrite() // этот метод записывает байт в регистр
{
   digitalWrite(latchEnable, LOW);   //Отключаем вывод на регистре
//так как у нас 4 регистра, то 3-м из них зададим нулевые(может быть другое) значения. Это необходимо, чтобы после каждого перезапуска программы из первого регистра байты не “проталкивались” во второй и т.д. циклически.
   shiftOut(datapin, clockpin, MSBFIRST, data_0);// "проталкиваем" байты в регистры
   shiftOut(datapin, clockpin, MSBFIRST, data_0);// "проталкиваем" байты в регистры
   shiftOut(datapin, clockpin, MSBFIRST, data_0);// "проталкиваем" байты в регистры
   shiftOut(datapin, clockpin, MSBFIRST, data_0);// "проталкиваем" байты в регистры   
   digitalWrite(latchEnable, HIGH);// "защелкиваем" регистр, чтобы биты появились на выходах регистра
}
