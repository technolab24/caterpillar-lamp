 /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 6.
    Управление машинкой по двум датчикам расстояния до бортов
    Автор Еличева Евгения
     */
 
    const int Trig1 = 1;
    const int Echo1 = 2;
    const int Trig2 = 3;
    const int Echo2 = 4;
    const int ledPin = 11;  // 13 – если будете использовать встроенный в Arduino светодиод

    // подключите пины контроллера к цифровым пинам Arduino
// первый двигатель
int enA = 10;
int in1 = 9;
int in2 = 8;
// второй двигатель
int enB = 5;
int in3 = 7;
int in4 = 6;

    void setup()
    {
      // инициализируем все пины для управления двигателями как outputs
    pinMode(enA, OUTPUT);
    pinMode(enB, OUTPUT);
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);
    
    pinMode(Trig1, OUTPUT);
    pinMode(Echo1, INPUT);
    pinMode(Trig2, OUTPUT);
    pinMode(Echo2, INPUT);
    pinMode(ledPin, OUTPUT);
     
    Serial.begin(9600);
    }

    unsigned int time_us2=0;
    unsigned int distance_sm2=0;
    unsigned int time_us1=0;
    unsigned int distance_sm1=0;
    void loop()
    {
    digitalWrite(Trig1, HIGH); // Подаем сигнал на выход микроконтроллера
    delayMicroseconds(10); // Удерживаем 10 микросекунд
    digitalWrite(Trig1, LOW); // Затем убираем
    time_us1=pulseIn(Echo1, HIGH); // Замеряем длину импульса
    distance_sm1=time_us1/58; // Пересчитываем в сантиметры
    Serial.println(distance_sm1); // Выводим на порт
      digitalWrite(Trig2, HIGH); // Подаем сигнал на выход микроконтроллера
    delayMicroseconds(10); // Удерживаем 10 микросекунд
    digitalWrite(Trig2, LOW); // Затем убираем
    time_us2=pulseIn(Echo2, HIGH); // Замеряем длину импульса
    distance_sm2=time_us2/58; // Пересчитываем в сантиметры
    Serial.println(distance_sm2); // Выводим на порт

    // эта функция обеспечивает работу двигателей во всем диапазоне возможных скоростей
// обратите внимание, что максимальная скорость определяется самим двигателем и напряжением питания
// и зависят от вашей платы управления
// запускают двигатели
digitalWrite(in1, LOW);
digitalWrite(in2, HIGH);
digitalWrite(in3, LOW);
digitalWrite(in4, HIGH);

// ускорение от нуля до максимального значения

for (int i = 0; i < 256; i++)
{
analogWrite(enA, i);
analogWrite(enB, i);
delay(20);
}
// торможение от максимального значения к минимальному
for (int i = 255; i >= 0; --i)
{
analogWrite(enA, i);
analogWrite(enB, i);
delay(20);
}
// теперь отключаем моторы
digitalWrite(in1, LOW);
digitalWrite(in2, LOW);
digitalWrite(in3, LOW);
digitalWrite(in4, LOW);

    if (distance_sm1>5 && distance_sm2>5) // Если расстояние менее 50 сантиметром
    {
        digitalWrite(ledPin, 1); // Зажигаем светодиод
       // запуск двигателя A
        digitalWrite(in1, HIGH);
        digitalWrite(in2, LOW);
        // устанавливаем скорость 200 из доступного диапазона 0~255
        analogWrite(enA, 200);
        // запуск двигателя B
        digitalWrite(in3, HIGH);
        digitalWrite(in4, LOW);
        // устанавливаем скорость 200 из доступного диапазона 0~255
        analogWrite(enB, 200);
        delay(2000);
    }
     if (distance_sm1<=5 && distance_sm2>=5) // Если расстояние менее 50 сантиметром
   // меняем направление вращения двигателей

    
    {
       digitalWrite(ledPin, 0); // иначе тушим
        digitalWrite(in1, LOW);
        digitalWrite(in2, HIGH);
        digitalWrite(in3, HIGH);
        digitalWrite(in4, LOW);
        
        delay(2000);
    }
       if (distance_sm1>=5 && distance_sm2<=5) // Если расстояние менее 50 сантиметром
   // меняем направление вращения двигателей

    
    {
        digitalWrite(ledPin, 0); // иначе тушим
        digitalWrite(in1, HIGH);
        digitalWrite(in2, LOW);
        digitalWrite(in3, LOW);
        digitalWrite(in4, HIGH);
        
        delay(2000);
    }
    if (distance_sm1==5 && distance_sm2==5) // Если расстояние менее 50 сантиметром
        {
          digitalWrite(in1, LOW);
          digitalWrite(in2, LOW);
          digitalWrite(in3, LOW);
          digitalWrite(in4, LOW);}
           
    delay(100);
    }

