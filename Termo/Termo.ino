 /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 7.
    Программа для демонстрации термометра, используя датчик температуры и шаговый двигатель
    Автор Еличева Евгения
     */

#include <DHT.h>


#define DHTPIN A0 // номер пина, к которому подсоединен датчик
DHT dht(DHTPIN, DHT11);
int buttonPin = 6;
int pins[] = {2, 3, 4, 5}; //Задаем пины по порядку
bool motorPhases[8][4] = { // [phase][pin] 
  // -------- pins ---------- 
  // Winding    A  B  A  B 
  // Motor Pin  1  2  3  4 
  // Color      Bl Pi Ye Or 
  { 1, 1, 0, 0}, 
  { 0, 1, 0, 0}, 
  { 0, 1, 1, 0}, 
  { 0, 0, 1, 0}, 
  { 0, 0, 1, 1}, 
  { 0, 0, 0, 1}, 
  { 1, 0, 0, 1}, 
  { 1, 0, 0, 0}
  };

 int phase = 0; 
 int _step = 1; // Если у шага поменять знак, на -1 - изменится направление вращения. 
 //int delaytime=1000; //В микросекундах для более точного регулирования скорости


void setup() {
  Serial.begin(9600);
  dht.begin();
   for (int i = 0; i < 4; i++) pinMode(pins[i], OUTPUT); 
   pinMode(buttonPin, INPUT); 
  
   }

void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
 bool isButtonDown = digitalRead(buttonPin); 
 Serial.println(isButtonDown); 
 if (isButtonDown) 
 {// _step = -_step;
  _step = 0;
  
// delay(500); // Делаем паузу, чтобы отсеч дребезг 
 }
 phase += _step; 
 if (phase > 7) phase = 0; 
 if (phase < 0) phase = 7; 
 for (int i = 0; i < 4; i++) 
 { digitalWrite(pins[i], ((motorPhases[phase][i] == 1) ? HIGH : LOW));
 }
  Serial.println(_step);
// Проверка удачно прошло ли считывание.
 /* if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
  }*/
}
