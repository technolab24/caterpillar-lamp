 /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 6.
    Энкодер
    Для управлением яркостью LED используется энкодер Sparkfun
    Автор Еличева Евгения
     */
  
int brightness = 120;       // яркость LED, начинаем с половины
int fadeAmount = 10;        // шаг изменения яркости LED
unsigned long currentTime;
unsigned long loopTime;
const int pin_A = 12;       // pin 12
const int pin_B = 11;       // pin 11
unsigned char encoder_A;
unsigned char encoder_B;
unsigned char encoder_A_prev=0;
 
void setup()  {
  // declare pin 9 to be an output:
  pinMode(9, OUTPUT);         // устанавливаем pin 9 как выход
  pinMode(8, OUTPUT);         // устанавливаем pin 9 как выход
  pinMode(pin_A, INPUT);
  pinMode(pin_B, INPUT);
  currentTime = millis();
  loopTime = currentTime; 
} 
 
void loop()  {
  currentTime = millis();
  if(currentTime >= (loopTime + 5)){ // проверяем каждые 5мс (200 Гц)
    encoder_A = digitalRead(pin_A);     // считываем состояние выхода А энкодера 
    encoder_B = digitalRead(pin_B);     // считываем состояние выхода B энкодера    
    if((!encoder_A) && (encoder_A_prev)){    // если состояние изменилось с положительного к нулю
      if(encoder_B) {
        // выход В в полож. сост., значит вращение по часовой стрелке
        // увеличиваем яркость, не более чем до 255
       
        if(brightness + fadeAmount <= 255){
          brightness += fadeAmount;     
         analogWrite(9, brightness);   // устанавливаем яркость на 9 ножку    
        }       
        }
      }   
      else {
        // выход В в 0 сост., значит вращение против часовой стрелки     
        // уменьшаем яркость, но не ниже 0
        if(brightness - fadeAmount >= 0){ brightness -= fadeAmount;   
      analogWrite(8, brightness); 
        }// устанавливаем яркость на 9 ножку            
      }   
 
    }   
    encoder_A_prev = encoder_A;     // сохраняем значение А для следующего цикла 
     
   // analogWrite(9, brightness);   // устанавливаем яркость на 9 ножку
    
    loopTime = currentTime;
  }                       

