 /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 6.
    Светофор
    Автор Еличева Евгения
     */
 

//объявляем переменные с номерами пинов

int r = 13;

int g = 12;

int b = 11;

void setup() //процедура setup

{

//объявляем используемые порты

pinMode(r, OUTPUT);

pinMode(g, OUTPUT);

pinMode(b, OUTPUT);

}

void loop() //процедура loop

{

digitalWrite(r, HIGH); //включаем красный

delay(500); //ждем 500 Мс

digitalWrite(r, LOW); //выключаем красный

digitalWrite(g, HIGH); //включаем зеленый

delay(500); //ждем 500 Мс

digitalWrite(g, LOW); //выключаем зеленый

digitalWrite(b, HIGH); //включаем синий

delay(500); //ждем 500 Мс

digitalWrite(b, LOW); //выключаем синий

}
