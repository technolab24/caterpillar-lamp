#include <NewPing.h>





NewPing sonar(8, 9, 110); 

const int datapin = 7; // SPI Master Out Slave In, connected to MBI5168 SDI
const int clockpin = 5; // Clock pin, connected to MBI5168 CLK
const int latchEnable = 6; // connected to MBI5168 LE
const int outputEnable = 3; // connected to MBI5168 OE, note: is PWM pin
const int rExt = 4; // R-EXT to MBI5168 OE, note: яркость ламп
const int light = 0; // инфракрасный датчик

byte byte_0 = 0;
byte byte_1 = 0;
byte byte_2 = 0;
byte byte_3 = 0;
int last = 32;

//DistanceGP2Y0A41SK DstLight;
int DstLightVal;
int lastLt;

const int Trig = 8; 
const int Echo = 9;
const int stap = 3;
unsigned int time_us = 0;
int distance_sm = 0;
int start_distance_sm = -1;
int last_distance_sm = -1;
int mod = 0;
int OperMOD=0; //текущее состояние лампы 0-горит, 1-управление сегментами, 2-управление яркостью
unsigned long DstSensTIM=0;
unsigned long OperTIM=0;
unsigned long SerOutTIM=0;
unsigned long MOD0TIM=0;
unsigned long MOD1TIM=0;
unsigned long MOD2TIM=0;
int DSTArr [10]; //массив измерений
int SgtArr [32]; //массив сегментов
int SgtLen=32;  //длинна лампы
int MOD1in=0; //данные в каком месте вошли в управление
int MOD1out=0; //данные о том в каком месте вышли из управления
int MOD2in=0; //данные в каком месте вошли в управление
int MOD2out=0; //данные о том в каком месте вышли из управления
int vector=0;//направление движения рукой 1-от датчика -1 к датчику
int NeedVector=0;
int NeedMOD2=0;
int LGTval=100;
int LGTvalOLD=0;


void setup(void) {
  
  Serial.begin(9600);

  // Параметры MBI5168
  pinMode(datapin, OUTPUT);
  pinMode(clockpin, OUTPUT);
  pinMode(latchEnable, OUTPUT);
  pinMode(outputEnable, OUTPUT);
  pinMode(rExt, OUTPUT); 
  analogWrite(rExt, 250); // Зададим яркость лампы (min:255, max:0), при включении на минимуме

  //  Звуковой датчик - определяет какой сектор лампы будем включать
  pinMode(Trig, OUTPUT); 
  pinMode(Echo, INPUT); 
  for (int i=0; i<SgtLen; i++){
    SgtArr [i]=0; //инициализация массива сегментов
  }
    for (int i=0; i<10; i++){
    DSTArr [i]=1024; //инициализация массива измерений, чтобы у нас не было краевого эффекта при включении
  }
Serial.println("start");
}


void loop(void) {
  
   
 if (DstSensTIM<millis()){ //если пришла пора замерить расстояние то слушаем датчик
  distance_sm = sonar.ping() / US_ROUNDTRIP_CM;
  if (distance_sm==0) distance_sm=1024;
  Serial.println(distance_sm);
  DstSensTIM=millis()+100; //слушаем 5 раз в секунду
if ((distance_sm!=0)){
    for (int i=9; i>0; i--){
    DSTArr[i]=DSTArr[i-1]; 
        }
   if (distance_sm<110){ //Убираем ошибки датчика  
      if (DSTArr[0]-distance_sm>6) {DSTArr[0]=DSTArr[0]-6;}
      else if (DSTArr[0]-distance_sm<-6) {DSTArr[0]=DSTArr[0]+6;}
      else {DSTArr[0]=distance_sm;}
       }
    DSTArr[0]=distance_sm;
    }
 } 
 
if ((OperMOD==0)&&(getavgDST(3)<110)&&(NeedVector==0)) { //переход  к управлению сегментами
    MOD1in=getavgDSTclear(3); //засекли место входа в режим управления
    MOD2in=MOD1in;
    MOD2out=MOD2in;
    MOD1TIM=millis()+300;//засекли время для определения вектора
    MOD2TIM=millis()+2000;//засекли время для перехода в мод2
    NeedVector=1; //Нужен вектор движения
    NeedMOD2=1; //Можем попасть в режим 2
    MOD1out=MOD1in;
    Serial.print("PRE MOD 1  ");
    }
if (((OperMOD==1)||(OperMOD==2))&&(getavgDST(9)>900)) { //переход в режим 0
    OperMOD=0;
    vector=0;
    Serial.println("MOD 0");
    Serial.print("MOD 1 out ");
    Serial.println(MOD1out);

    pulse (3);
  }
  
if ( (MOD1TIM<millis())&&(NeedVector==1)){//вычисляем вектор движения. Берем место входа и сравниваем с последними двумя замерам
      if (getavgDSTclear(2)<(MOD1in-3)) {
        vector=-1;
        } else if (getavgDSTclear(2)>(MOD1in+3)) {
         vector=1;
         } 
       Serial.print("MOD 1 in ");
       Serial.println(MOD1in);
       Serial.print("vector  ");
       Serial.println(vector);
 
 //      pulse(1);
       OperMOD=1;
       NeedVector=0;
       }
       
if ( (MOD2TIM<millis())&&(NeedMOD2==1)){//вычисляем попытку перейти в МОД2 (1 секунда без движения руки)
    if (getavgDSTclear(3)<(MOD1in-5)) {
      vector=-1;
    } else if (getavgDSTclear(3)>(MOD1in+5)) {
       vector=1;
       } else {
        OperMOD=2; 
        Serial.println("MOD 2"); 
        pulse(2);
       }
       NeedMOD2=0;
}
if ((OperMOD==1) || (OperMOD==2)){//Управляем лампой
  if (DSTArr[0]<110){ //если не вышли, то скользим средним, убирая неверные данные
 /* //попытка найти вектор в процессе. не удачна
   if ((OperMOD==1) && (vector==0)){
         if (DSTArr[0]>MOD1out) vector=1; 
           else vector=-1;
            Serial.print("vector  ");
            Serial.println(vector);
         }
  */
   if (vector==-1) {if (DSTArr[0]<MOD1out)  {MOD1out=(MOD1out+DSTArr[0]*2)/3;}}
   if (vector==1) {if (DSTArr[0]>MOD1out)  {MOD1out=(MOD1out+DSTArr[0]*2)/3;}}
   MOD2out=(MOD2out+DSTArr[0]*2)/3;
   }
}  

if (OperTIM<millis()){//производим расчеты и управление лампой
   if (OperMOD==2){  //Управляем яркостью
         LGTval=LGTval+(MOD2out-MOD2in)*3;
         MOD2in=MOD2out;
         if (LGTval>250) LGTval=250;
         if (LGTval<5) LGTval=5;
         analogWrite(rExt, 255-LGTval);
             Serial.print("LGTval  ");
            Serial.println(LGTval);

         }
    if (OperMOD==1){     
          if (MOD1out>31*3) MOD1out=31*3;
          if (MOD1in>31*3) MOD1in=31*3;
         
          if (MOD1out>MOD1in){
              for (int i=(MOD1in/3); i<=(MOD1out/3); i++) SgtArr [i]=1;
              }
          if (MOD1out<MOD1in){
              for (int i=(MOD1in/3); i>=(MOD1out/3); i--) SgtArr [i]=0;
              }
          byte_0=0;
          byte_1=0;
          byte_2=0;
          byte_3=0;
          
          for (int i=7; i>-1; i--){// заполняем байты для сегментов
            byte_0=byte_0<<1;
            byte_0=byte_0+SgtArr [i];
            byte_1=byte_1<<1;
            byte_1=byte_1+SgtArr [i+8];
            byte_2=byte_2<<1;
            byte_2=byte_2+SgtArr [i+16];
            byte_3=byte_3<<1;
            byte_3=byte_3+SgtArr [i+24];
          }
    
          SgtWrite() ; //Елочка зажгись!
          }
    
         OperTIM=millis()+100;
     }
/* 
if (SerOutTIM<millis()){ // раз в секунду выводим диагностику
    for (int i=0; i<10; i++){
    Serial.print(DSTArr[i]);
    Serial.print("  ");
    }
    Serial.println(getavgDST(5));
    SerOutTIM=millis()+1000;
    Serial.println(OperMOD);
    Serial.print("MOD1in  ");
    Serial.println(MOD1in/3);
    Serial.print("MOD1out  ");
    Serial.println(MOD1out/3);
     Serial.print("MOD2in  ");
  Serial.println(MOD2in);
    Serial.print("MOD2out  ");
    Serial.println(MOD2out);
    Serial.print("vector  ");
    Serial.println(vector);
 
     Serial.print("LGTval  ");
    Serial.println(LGTval);
   Serial.print("bytes  ");
      Serial.print(byte_0);
      Serial.print(byte_1);
      Serial.print(byte_2);
      Serial.println(byte_3);
    for (int i=0; i<SgtLen; i++){
    Serial.print(SgtArr [i]); //вывод массива сегментов
  }
     Serial.println("  ");

 }
 */
 
 
 
}

int getDistance() {
  
  digitalWrite(Trig, HIGH); // Подаем сигнал на выход микроконтроллера 
  delayMicroseconds(10); // Удерживаем 10 микросекунд 
  digitalWrite(Trig, LOW); // Затем убираем 
  time_us = pulseIn(Echo, HIGH); // Замеряем длину импульса   
  return time_us/58; // Пересчитываем в сантиметры   
  
}

int getavgDST (int n){ //считаем среднюю дистанцию по n измерениям
int avg=0;
 for (int i=0; i<n; i++){
   avg=avg+DSTArr[i];
 }
 return avg/n;
 }
   
   int getavgDSTclear (int n){ //считаем среднюю дистанцию по n измерениям, выкидывая сбои в измерении
int avg=0;
int cnt=0;
 for (int i=0; i<n; i++){
   if (DSTArr[i]<120){
   avg=avg+DSTArr[i];
   cnt++;
   }
 }
 return avg/cnt;
 }
   
   
void pulse (int num){     //Мигаем первым сегментом NUM раз
 
 
  for (int i=1; i<=num; i++){
         analogWrite(rExt, 255);
         delay (50);
         analogWrite(rExt, 255-LGTval);
         delay (50);
        }
 
}
  
void lamp2() {
  distance_sm = getDistance();
  Serial.println(distance_sm);
}


void SgtWrite() 
{

  digitalWrite(outputEnable, LOW);
  digitalWrite(latchEnable, LOW);
  shiftOut(datapin, clockpin, MSBFIRST, byte_3);
  shiftOut(datapin, clockpin, MSBFIRST, byte_2);
  shiftOut(datapin, clockpin, MSBFIRST, byte_1);
  shiftOut(datapin, clockpin, MSBFIRST, byte_0);
  digitalWrite(latchEnable, HIGH);
  digitalWrite(latchEnable, LOW);
  digitalWrite(outputEnable, LOW);
  
}





