  /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 2.
    Программа для демонстрации на лампе Катерпилляр возможности управления датчиком движения. Два режима: рабочая яркость, дежурная яркость.
    Автор Еличева Евгения
     */


const int datapin = 7; // Вход последовательной шины данных
    const int clockpin = 5; //   Вход тактовой частоты
    const int latchEnable = 6; // Выход данных
    const int outputEnable = 3; // Разрешение выхода
    const int rExt = 4; // Программирование тока (внешний резистор) работает в инверсном режиме.
    const int datch = 18; // подключение датчика движения
    const int LedPin=13;
  unsigned long time;
  long TimSerial=0;//таймер отладки
    
     int mass[32];
    int SgtLen=32;  //длинна лампы
    int SgtArr [32]; // массив сегментов
    long TimWork=0;// переменная для счетчика рабочего режима
    long TimSB =0;// переменная для счетчика дежурного режима

    long TW=10000; // задержка рабочего режима, после последнего срабатывания
    long TSB=20000;//задержка дежурного режима, после последнего срабатывания
    int light=0;
    void setup()
    {
      for (int i=0; i<SgtLen; i++){
    SgtArr [i]=1; //инициализация массива сегментов
    }
//см. Блок 1. Урок 3.
    pinMode(LedPin, OUTPUT);
    pinMode(datch, INPUT);
    pinMode(datapin, OUTPUT);
    pinMode(clockpin, OUTPUT);
    pinMode(latchEnable, OUTPUT);
    pinMode(outputEnable, OUTPUT);
    pinMode(rExt, OUTPUT);
    Serial.begin(9600);//инициализация работы с последовательным портом. Данная команда необходима для отладки программы. Просмотр осуществляется в мониторе порта.

      SgtWrite();
}
   
 
     void loop() {      

long pirVal=digitalRead(datch);//присваиваем значение переменной, digitalRead()- функция считывает значение с заданного входа

 
if (pirVal == HIGH){// если датчик поймал движение
 TimWork=millis()+TW;//включаем счетчик на рабочем режиме
 TimSB=millis()+TSB;//и дежурном режиме
 }
 light=255; //задаем яркость лампы равное нулю(т.к. у нас инвертор, то записываем обратное значение)
 if(TimSB>millis()){//если время дежурного режима больше счетчика начала работы программа
 light=200;
 
  }
  if(TimSB==millis()){//если если время дежурного режима равно счетчику начала работы 
  for(light=200;light<=255;light++){ //плавное угасание лампы с 20% до 0%
     analogWrite(rExt,light);
     delay(10);
    }
  }
  if(TimWork>millis()){//если время рабочего режима больше счетчика начала работы программы
    light=0;
 
    }
       if(TimWork==millis()){//если если время рабочего режима равно счетчику начала работы 

  for(light=0;light<=200;light++){//плавное угасание лампы с максимальной яркости до 20%
     analogWrite(rExt,light);
     delay(10);
  }
    }
   analogWrite(rExt,light); //передаем наш параметр на порт выхода


if(TimSerial<millis()){//запускаем таймер для отладки, если значение таймера меньше счетчика
  TimSerial=millis()+1000;//включаем счетчик через секунду
  Serial.print("Pir state ");
   Serial.println(pirVal);
     Serial.print("Led state ");
   Serial.println(light);
  }   
     }
          
            
    
void SgtWrite() // эта процедура записывает байты в сдвиговые регистры
{
  digitalWrite(latchEnable, LOW); //Отключаем вывод на регистре
  shiftOut(datapin, clockpin, MSBFIRST,255); // "проталкиваем" байты в регистры, так как у нас 4 сдвиговых регистра, то чтобы заполнить их все нам надо протолкнуть за раз 4 байта.
  shiftOut(datapin, clockpin, MSBFIRST, 255); // "проталкиваем" байты в регистры
  shiftOut(datapin, clockpin, MSBFIRST, 255); // "проталкиваем" байты в регистры
  shiftOut(datapin, clockpin, MSBFIRST, 255); // "проталкиваем" байты в регистры
  digitalWrite(latchEnable, HIGH); //"защелкиваем" регистр, чтобы биты появились на выходах регистра

 
}

