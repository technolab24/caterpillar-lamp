       /* Технолаб24.
    Школа Технолаб24 Блок 1 Занятие 4.
    Программа для демонстрации на лампе Катерпилляр возможности управления яркостью, используя датчик дальномер
    Автор Еличева Евгения
     */

    const int datapin = 7; // Вход последовательной шины данных
    const int clockpin = 5; //   Вход тактовой частоты
    const int latchEnable = 6; // Выход данных
    const int outputEnable = 3; // Разрешение выхода
    const int rExt = 4; // Программирование тока (внешний резистор) работает в инверсном режиме.
    const int light = 0; // инфракрасный датчик
    int DSTArr [10]; //массив измерений
    byte byte_0 = 0;
    byte byte_1 = 0;
    byte byte_2 = 0;
    byte byte_3 = 0;
    int SgtLen=32;  //длинна лампы
    char inputString[4];
    int SgtArr [32];
    int Trig=0;
    int Echo=0;
    
    void setup()
    {
     for (int i=0; i<SgtLen; i++){
    SgtArr [i]=1; //инициализация массива сегментов
    }
    
    pinMode(datapin, OUTPUT);
    pinMode(clockpin, OUTPUT);
    pinMode(latchEnable, OUTPUT);
    pinMode(rExt, OUTPUT);
    
    //analogWrite(rExt,250); // Зададим яркость лампы (min:255, max:0), при включении на минимуме
    Serial.begin(9600);
    Serial.println("reset");
}
    unsigned int time_us=0;//переменная для замера длины

    unsigned int distance_sm=0;//переменная для замера длины в см.
   

    void loop() {
    digitalWrite(Trig, HIGH); // Подаем сигнал на выход микроконтроллера
    delayMicroseconds(10); // Удерживаем 10 микросекунд
    digitalWrite(Trig, LOW); // Затем убираем
    time_us=pulseIn(Echo, HIGH); // Замеряем длину импульса
    distance_sm=time_us/58; // Пересчитываем в сантиметры
    delay(100);
   distance_sm=map(distance_sm,0,100,0,255);/*Функция пропорционально переносит значение (distance_sm)
   из текущего диапазона значений (0 и 100) в новый диапазон (о и 255), заданный параметрами.*/
    if(distance_sm>255)
    {
      distance_sm=255;}
      
    distance_sm=floor((distance_sm/10)*10);// округлим для равномерного свечения лампы
    analogWrite(rExt,distance_sm);// передадим данные на порт
    Serial.println(distance_sm); // Выводим на порт

             for(int k=7;k>-1; k--){ // заполняем байты для сегментов
        
          byte_0=byte_0<<1; //сдвиг байтов влево, таким образом мы проталкиваем в байт биты из массива
            byte_0=byte_0+SgtArr [k];// заполнение младшего бита из массива
            byte_1=byte_1<<1;
            byte_1=byte_1+SgtArr [k+8];
            byte_2=byte_2<<1;
            byte_2=byte_2+SgtArr [k+16];
            byte_3=byte_3<<1;
            byte_3=byte_3+SgtArr [k+24];
    
          SgtWrite() ; //Елочка зажгись!
          

   
    }
 
    
 
void SgtWrite() // эта процедура записывает байты в сдвиговые регистры
{
digitalWrite(latchEnable, LOW);//Отключаем вывод на регистре
  shiftOut(datapin, clockpin, MSBFIRST, byte_3);// "проталкиваем" байты в регистры
  shiftOut(datapin, clockpin, MSBFIRST, byte_2);
  shiftOut(datapin, clockpin, MSBFIRST, byte_1);
  shiftOut(datapin, clockpin, MSBFIRST, byte_0);
  digitalWrite(latchEnable, HIGH); //"защелкиваем" регистр, чтобы биты появились на выходах регистра
 
}
